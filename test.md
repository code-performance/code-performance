A microwave oven is an electrical appliance designed for cooking, heating and defrosting food. A microwave can be found in almost every kitchen. Despite the simplicity of the device, the choice of such a technique should be treated responsibly, having previously studied the main parameters.
Installation type
Built-in microwave oven . Fits into a kitchen unit, maintaining the unity of the style of the room.
Freestanding. Can be placed on brackets, table, shelf or in one of the drawers.
Internal volume of the chamber
The chamber volume can be from 17 to 42 liters, depending on the model. Spacious microwave ovens are suitable for a large family. Compact microwave ovens can easily cope with heating food, making hot sandwiches or defrosting convenience foods.
microwave oven
Power
This parameter determines the speed of defrosting and cooking. Depending on the task, the microwave should work at a certain power. To keep the food warm, 100–150 W is enough, and 900 W for defrosting. To prepare a full meal, the power must be up to 1400 W. Most modern models have a power of 700 to 1000 watts.
When purchasing a microwave oven, it is necessary to take into account the condition of the electrical wiring, since powerful models can create additional load on the network.
Internal coating
Most often, the inner surface of the microwave is covered with one of three types of material:
Enamel coating. These microwave ovens are reliable, durable, inexpensive, and easy to clean. However, in the process of work, cracks and chips may appear.
Stainless steel cover. It is characterized by resistance to high temperatures, low price. The disadvantages include the difficulty in cleaning, since there are traces of drops and fingers on the metal.
Bioceramic coating. An economical, durable material that is easy to clean and reduces energy costs. When working with such a microwave oven, one must be careful, since bioceramics are quite fragile.
Control
There are two types of microwave control: mechanical and electronic.
Mechanical control is provided by rotary switches, most often used in low-cost models. They are characterized by low cost, ease of management, low probability of damage.
There are three types of electronic control:
Keypad. Has an attractive design and the ability to change programs. Requires frequent cleaning, as dirt quickly accumulates around the buttons.
Touchpad. Easy to maintain and use, however, sensor elements are prone to breakage and are expensive.
Rotary pushbutton panel. The simplest and most proven option. The disadvantages include the inability to set the exact cooking time - https://kstech.com.ua/tovari-dlya-kyxni/ak-vybraty-mikrokhvylovu-pich-dlia-domu/
Grill function
For lovers of dishes with a "golden crust", microwave ovens with a grill are provided. They are of three types:
microwave oven
Heating element grill. The heating element (tubular electric heater) is the simplest, inexpensive type of grill, it can be rotated, providing heating from the side and from the top. It retains heat for a long time, bakes food well. At the same time, it is difficult to clean and requires a lot of energy.
Quartz Grill - A tubular quartz electric heater located behind a metal grill on the top of the stove. Economical, heats up and cooks quickly. Dishes cooked on such a grill taste good. A quartz lamp gets dirty quickly and is difficult to clean.
Infrared grill. The best option for crispy dishes. Usually there are several cooking modes in which dishes are prepared quickly, easily, tasty. The infrared grill can be used separately from the oven.
Microwaves with convection .
In such microwave ovens, food is cooked no worse than in ovens. Food placed on a skewer or wire rack is treated with a stream of hot air. Convection is a useful feature for lovers of baked goods and baked meat.
Reheating and defrosting
These are the most popular microwave functions used by consumers. In the microwave, you can defrost meat, fish, vegetables, berries, as well as reheat a previously prepared dish.
Additional features
In addition to the basic functions, there are a number of additional ones:
a double boiler (for preparing diet meals);
auto-cooking (built-in modes);
automatic weighing;
the program "My recipes";
quick start (heating food at maximum power in a couple of minutes);
function of double radiation (for uniform heating of dishes);
odor elimination mode;
self-cleaning (steam cleaning mode);
child lock;
step-by-step cooking function.
Dishes
When choosing cookware for a microwave oven, you need to pay attention to its thermal stability and fire resistance. Porcelain, ceramic, thermoplastic, and glassware are suitable for conventional ovens. Microwave ovens with grill and convection will require fireproof materials.
In addition, cymbals should be free of any metal or gilding.
When shopping for a microwave oven, remember two rules:
It is forbidden to use metal, crystal, melamine dishes;
It is allowed to use products from ceramics, porcelain, plastic, fabric, glass.
Benefits of microwaves
The microwave oven has long become a reliable assistant in the kitchen; cooking with it has the following advantages:
Heating and defrosting functions;
The ability to cook a complete meal;
Lack of fat, odor, soot;
When cooking without oil, food does not burn;
Economical power consumption.
Is it safe to use the microwave?
This is one of the main questions consumers are asking. According to the World Health Organization, microwave radiation does not adversely affect the human body.
Microwave radiation is not radioactive. When it penetrates food, it only affects water molecules, which leads to rapid heating of food. At the same time, vitamins and minerals are preserved in the dish.
There is a myth that when you work with a microwave you have to leave the kitchen. This is not necessary as microwaves are completely safe. In addition, the inner chamber, made of a metal housing, does not transmit radiation outside the microwave.
However, there are two restrictions on using the microwave:
In the presence of a pacemaker, do not approach the device more than 10 cm to avoid interference;
Do not use a damaged oven.
Following simple rules, observing safety measures, and using the right utensils will allow you to enjoy cooking in the microwave for many years.
